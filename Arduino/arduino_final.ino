#include <LiquidCrystal.h>
LiquidCrystal lcd(3, 4, 6, 7, 8, 9);

#include <AddicoreRFID.h>
#include <SPI.h>

#define  uchar unsigned char
#define uint  unsigned int

uchar fifobytes;
uchar fifoValue;

AddicoreRFID myRFID; // create AddicoreRFID object to control the RFID module

/////////////////////////////////////////////////////////////////////
//set the pins
/////////////////////////////////////////////////////////////////////
const int chipSelectPin = 10;
const int NRSTPD = 5;

//Maximum length of the array
#define MAX_LEN 16


String incoming;

int reset;

int MoveX;
int MoveY;

int anX;
int anY;

int ammo1 = 5, ammo2= 2, ammo3= 3, ammo4= 1;

int current_ammo = 1;

bool shot = false;

String ToUnity;

float tiempo = 0;

String key;

byte skill_1[8] = {
  B00000,
  B00000,
  B01110,
  B11111,
  B11111,
  B01110,
  B00000,
  B00000,
};
byte skill_2[8] = {
  B10001,
  B01110,
  B11011,
  B01010,
  B01010,
  B11011,
  B01110,
  B10001,
};
byte skill_3[8] = {
  B01110,
  B11011,
  B11011,
  B10001,
  B10001,
  B11011,
  B11011,
  B01110,
};
byte skill_4[8] = {
  B00100,
  B01110,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
};

void setup() {
  Serial.begin(9600);

  // start the SPI library:
  SPI.begin();
  
  pinMode(chipSelectPin,OUTPUT);              // Set digital pin 10 as OUTPUT to connect it to the RFID /ENABLE pin 
    digitalWrite(chipSelectPin, LOW);         // Activate the RFID reader
  pinMode(NRSTPD,OUTPUT);                     // Set digital pin 10 , Not Reset and Power-down
    digitalWrite(NRSTPD, HIGH);

  myRFID.AddicoreRFID_Init();


  
  lcd.createChar(1, skill_1);
  lcd.createChar(2, skill_2);
  lcd.createChar(3, skill_3);
  lcd.createChar(4, skill_4);
  lcd.begin(16,2);
  pinMode(2, INPUT);
  pinMode(A5, INPUT);
  pinMode(A4, OUTPUT);
  digitalWrite(A4, LOW);
  delay(100);
}
void loop() {

  ToUnity = "";

  ProcessReset();

  ProcessUltraSonido();

  ProcessRFID();

  ProcessJoystick();

  Serial.println(ToUnity);

  ProcessLCD();

}

void ProcessReset(){
  reset = digitalRead(2);

  if(reset == HIGH || FromUnity())
  {

    ammo1 = 5;
    ammo2 = 2;
    ammo3 = 3;
    ammo4 = 1;

    current_ammo = 1;

    ToUnity += "1";
  }
  else
    ToUnity += "0";
}

bool FromUnity(){
  if (Serial.available() > 0) {
    incoming = Serial.read();

    if (incoming == "102")
      return true;
    else if (incoming == "103")
      ammo1 += 1;
    else if (incoming == "104")
      ammo2 += 1;
    else if (incoming == "105")
      ammo3 += 1;
    else if (incoming == "106")
      ammo4 += 1;
  }

  return false;
}

void ProcessUltraSonido(){
  
  digitalWrite(A4, HIGH);
  delayMicroseconds(10);
  digitalWrite(A4, LOW);
  tiempo = pulseIn(A5, HIGH);
  
  if(tiempo < 1000)
  {
    shot = true;
    ToUnity += "1";
  }
  else
    ToUnity += "0";
    
}

void ProcessRFID(){
  
      uchar i, tmp, checksum1;
  uchar status;
        uchar str[MAX_LEN];
        uchar RC_size;
        uchar blockAddr;  //Selection operation block address 0 to 63
        String mynum = "";

        str[1] = 0x4400;
  //Find tags
  status = myRFID.AddicoreRFID_Request(PICC_REQIDL, str); 

  //Anti-collision, return tag serial number 4 bytes
  status = myRFID.AddicoreRFID_Anticoll(str);
  if (status == MI_OK)
  {
          key = "";
          checksum1 = str[0] ^ str[1] ^ str[2] ^ str[3];
          
          key += str[0];
          key += str[1];
          key += str[2];
          key += str[3];
         
          if (str[4] == checksum1)
          {
            if(key == "1158646")
            {
                current_ammo = 1;
            }
            else if(key == "4825423798")
            {
                current_ammo = 2;
            }
            else if(key == "29239238208")
            {
                current_ammo = 3;
            }
            else if(key == "138213139121")
            {
                current_ammo = 4;
            }
          }
  }
    
        myRFID.AddicoreRFID_Halt();
        
  ToUnity += current_ammo;
}
void ProcessJoystick(){
  anX = analogRead(A0);
  anY = analogRead(A1);

  if (anX > 800)
  {
    ToUnity += "2";
  }
  else if (anX < 200)
  {
    ToUnity += "0";
  }
  else
  {
    ToUnity += "1";
  }

  if (anY > 800)
  {
    ToUnity += "2";
  }
  else if (anY < 200)
  {
    ToUnity += "0";
  }
  else
  {
    ToUnity += "1";
  }
}
void ProcessLCD(){

  for (int i = 0; i < 24; i++)
  {
    lcd.setCursor(i,0);
    lcd.print(" ");
    lcd.setCursor(i,1);
    lcd.print(" ");
  }

  lcd.setCursor(0,0);
  lcd.print("Skill:");

  if (current_ammo == 1){
    if(shot && ammo1 >0) ammo1 -=1;

    lcd.print(" Cannon");
    Display_Bullets(byte(1), ammo1);
  }
  else if (current_ammo == 2){
    if(shot && ammo2 >0) ammo2 -=1;

    lcd.print(" Saw");
    Display_Bullets(byte(2), ammo2);
  }
  else if (current_ammo == 3){
    if(shot && ammo3 >0) ammo3 -=1;

    lcd.print(" Shield");
    Display_Bullets(byte(3), ammo3);
  }
  else if (current_ammo == 4){
    if(shot && ammo4 >0) ammo4 -=1;

    lcd.print(" Beam");
    Display_Bullets(byte(4), ammo4);
  }
  
  shot = false;

}

void Display_Bullets(char ct, int ammount){

  for(int i = 0; i < ammount; i++)
  {
    lcd.setCursor(i,1);
    lcd.print(ct);
  }
}
