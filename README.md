Este proyecto incorpora hardware especial a un juego de supervivencia creado en Unity.
El objetivo es llegar a la meta vivo evitando o derrotando enemigos que aparecerán en el camino.

El control del personaje se realiza con un Joystick, el disparo con un sensor de ultrasonido
y el cambio de habilidad con un módulo RFID. También, se pueden ver las cargas de tu habilidad
actualmente activa en una pantalla LCD. Todos estos dispositivos están conectados mediante un
Arduino UNO.